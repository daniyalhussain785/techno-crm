-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2022 at 07:01 PM
-- Server version: 5.7.37
-- PHP Version: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `designcr_user`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sign` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `url`, `status`, `created_at`, `updated_at`, `logo`, `auth_key`, `phone`, `phone_tel`, `email`, `address`, `address_link`, `sign`) VALUES
(1, 'Design Gene Pro', 'http://designsgenepro.com/', 1, '2022-03-02 04:18:14', '2022-03-02 04:18:14', 'uploads/brands/designs-gene-pro-logo.png', 'b79063d5b5513dbabae2acc669e24fdc3c4e0137', '+1 888 712 2880', 'tel:+18887122880', 'info@designsgene.com', '2021 Fillmore Street San Francisco, CA 94115', '#', '$'),
(2, 'Designs Cottage', 'https://designscottage.com/', 1, '2022-03-02 04:22:26', '2022-03-02 04:22:26', 'uploads/brands/designs-cottage-logo.png', 'bf754890e87d49a79ff5c3d1f6ff4f4b7fd49925', '+1 888 683 0780', 'tel:+18886830780', 'info@designscottage.com', '5342 Clark Road Sarasota, FL 34233, US, Sarasota, FL 34233', 'https://www.google.com/maps/dir//27.26882834009934,-82.46271718933576', '$'),
(3, 'The Designs Planet', 'https://thedesignsplanet.com/', 1, '2022-03-02 04:25:24', '2022-03-02 04:25:24', 'uploads/brands/design-planet-logo.png', '89ba9d8d0e7e9cac4186c187f0112658541756af', '+1 888 512 0705', 'tel:+18885120705', 'info@thedesignsplanet.com', '99 Hudson Street. Manhattan, NY 10013, USA', 'https://goo.gl/maps/LrD5NUGN3NBziFh96', '$');

-- --------------------------------------------------------

--
-- Table structure for table `brand_users`
--

CREATE TABLE `brand_users` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand_users`
--

INSERT INTO `brand_users` (`user_id`, `brand_id`, `created_at`, `updated_at`) VALUES
(2, 1, NULL, NULL),
(3, 1, NULL, NULL),
(5, 1, NULL, NULL),
(7, 1, NULL, NULL),
(8, 1, NULL, NULL),
(9, 1, NULL, NULL),
(10, 2, NULL, NULL),
(11, 2, NULL, NULL),
(12, 1, NULL, NULL),
(13, 1, NULL, NULL),
(14, 1, NULL, NULL),
(14, 2, NULL, NULL),
(15, 1, NULL, NULL),
(15, 2, NULL, NULL),
(16, 1, NULL, NULL),
(16, 2, NULL, NULL),
(17, 1, NULL, NULL),
(18, 1, NULL, NULL),
(19, 1, NULL, NULL),
(20, 1, NULL, NULL),
(21, 1, NULL, NULL),
(22, 1, NULL, NULL),
(23, 1, NULL, NULL),
(24, 2, NULL, NULL),
(25, 1, NULL, NULL),
(27, 2, NULL, NULL),
(28, 2, NULL, NULL),
(30, 3, NULL, NULL),
(31, 3, NULL, NULL),
(32, 3, NULL, NULL),
(33, 3, NULL, NULL),
(34, 3, NULL, NULL),
(35, 2, NULL, NULL),
(36, 2, NULL, NULL),
(39, 1, NULL, NULL),
(40, 1, NULL, NULL),
(45, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_project`
--

CREATE TABLE `category_project` (
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_users`
--

CREATE TABLE `category_users` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_users`
--

INSERT INTO `category_users` (`user_id`, `category_id`, `created_at`, `updated_at`) VALUES
(42, 1, NULL, NULL),
(42, 3, NULL, NULL),
(43, 1, NULL, NULL),
(43, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assign_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `last_name`, `email`, `contact`, `created_at`, `updated_at`, `user_id`, `status`, `brand_id`, `url`, `subject`, `service`, `message`, `stripe_token`, `assign_id`) VALUES
(1, 'Harry', 'Dev', 'harrydev96@gmail.com', '00000', '2022-03-04 05:37:21', '2022-03-04 05:37:21', 3, 1, 1, NULL, NULL, NULL, NULL, 'cus_LFuzKQW2NOl0Aw', NULL),
(2, 'test', 'Dev', 'test@gmail.com', '123', '2022-03-04 05:44:00', '2022-03-04 05:44:00', 1, 1, 1, NULL, NULL, NULL, NULL, 'cus_LFv7jvEtBHQQTi', NULL),
(3, 'harry', 'dev', 'harrydev966@gmail.com', '0123456789', '2022-03-04 06:53:30', '2022-03-04 06:53:30', 19, 1, 1, NULL, NULL, NULL, NULL, 'cus_LFwEqZ3YwA5CZw', NULL),
(4, 'Daphne', 'Mayo', 'daph@gmail.com', '123131', '2022-03-05 01:16:44', '2022-03-05 01:16:44', 28, 1, 2, NULL, NULL, NULL, NULL, 'cus_LGE1SlTUfSRK7q', NULL),
(5, 'test', 'check', 'test3@gmail.com', '1000000', '2022-03-05 05:37:43', '2022-03-05 05:37:43', 35, 1, 2, NULL, NULL, NULL, NULL, 'cus_LGIED3lEbXmI72', NULL),
(8, 'Claire', 'Langan', 'clangan@tpi.ca', '2892317905', '2022-03-08 04:00:01', '2022-03-08 04:00:01', 31, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'customer', 'testa', 'musab.technifiedlabs@gmail.com', '7777777', '2022-03-08 05:04:31', '2022-03-08 05:12:58', 39, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Lisandra', 'Mendoza', 'lisandramendoza1987@gmail.com', '7868383739', '2022-03-09 23:53:46', '2022-03-09 23:53:46', 31, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'test', 'test', 'test321@gmail.com', NULL, '2022-03-26 04:38:00', '2022-03-26 04:38:00', 31, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'test', 'One', 'testoneone@gmail.com', NULL, '2022-03-26 06:50:20', '2022-03-26 06:50:20', 31, 1, 3, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_files`
--

CREATE TABLE `client_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `task_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_check` tinyint(4) NOT NULL,
  `production_check` tinyint(4) NOT NULL DEFAULT '0',
  `message_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `content_writing_forms`
--

CREATE TABLE `content_writing_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_industry` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_products` text COLLATE utf8mb4_unicode_ci,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `competitor` text COLLATE utf8mb4_unicode_ci,
  `company_business` text COLLATE utf8mb4_unicode_ci,
  `customers_accomplish` text COLLATE utf8mb4_unicode_ci,
  `company_sets` text COLLATE utf8mb4_unicode_ci,
  `mission_statement` text COLLATE utf8mb4_unicode_ci,
  `existing_taglines` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `content_writing_forms`
--

INSERT INTO `content_writing_forms` (`id`, `company_name`, `company_details`, `company_industry`, `company_reason`, `company_products`, `short_description`, `keywords`, `competitor`, `company_business`, `customers_accomplish`, `company_sets`, `mission_statement`, `existing_taglines`, `user_id`, `invoice_id`, `agent_id`, `created_at`, `updated_at`) VALUES
(1, 'Ortiz and Long Inc', 'Unde incididunt sunt', 'Sint commodi quasi u', 'Eum qui sunt aliquid', 'Sapiente dignissimos', 'Occaecat doloremque', 'Animi cillum evenie', 'Aute ab dolorum sit', 'Occaecat mollit rem', 'Minus aliquip quisqu', 'Quibusdam alias dolo', 'Provident dolor ist', 'Amet adipisicing au', 37, 6, 35, '2022-03-05 05:40:22', '2022-03-05 05:44:26'),
(2, 'Collier Stanton Co', 'Dolores numquam earu', 'Commodi eligendi eos', 'Natus odit eligendi', 'Aut omnis voluptas m', 'Voluptatibus dicta q', 'Tempora quis quas as', 'Sunt eos sit ullam', 'Error labore quidem', 'Dolor perferendis mi', 'In eligendi qui volu', 'Dolore incididunt et', 'Quibusdam velit libe', 41, 9, 39, '2022-03-08 05:14:42', '2022-03-08 05:16:46');

-- --------------------------------------------------------

--
-- Table structure for table `create_categories`
--

CREATE TABLE `create_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `create_categories`
--

INSERT INTO `create_categories` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Logo Design', 1, '2022-03-04 05:34:22', '2022-03-04 05:34:22'),
(2, 'Frontend', 1, '2022-03-04 05:34:34', '2022-03-04 05:34:34'),
(3, 'Backend', 1, '2022-03-04 05:34:39', '2022-03-04 05:34:39'),
(4, 'Content Writing', 1, '2022-03-04 05:34:48', '2022-03-04 05:34:48'),
(5, 'Search Engine Optimization', 1, '2022-03-04 05:34:55', '2022-03-04 05:34:55');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sign` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `short_name`, `sign`, `created_at`, `updated_at`) VALUES
(1, 'Dollar', 'usd', '$', '2022-03-02 04:28:48', '2022-03-02 04:28:48'),
(2, 'Canadian dollar', 'cad', 'CA$', '2022-03-02 04:31:47', '2022-03-02 04:31:47'),
(3, 'Pound sterling', 'gbp', '£', '2022-03-02 04:33:16', '2022-03-02 04:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `form_files`
--

CREATE TABLE `form_files` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `logo_form_id` bigint(20) UNSIGNED DEFAULT NULL,
  `form_code` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `form_files`
--

INSERT INTO `form_files` (`id`, `path`, `name`, `status`, `logo_form_id`, `form_code`, `created_at`, `updated_at`) VALUES
(2, 'salad-new-620_620x350_41478595911_0_1646354392.jpg', 'salad-new-620_620x350_41478595911', 1, 1, 2, '2022-03-04 05:39:52', '2022-03-04 05:39:52'),
(3, 'slider4_0_1646354871.jpg', 'slider4', 1, 3, 2, '2022-03-04 05:47:51', '2022-03-04 05:47:51'),
(4, 'spoon1_0_1646354925.png', 'spoon1', 1, 2, 2, '2022-03-04 05:48:45', '2022-03-04 05:48:45'),
(5, '1_0_1646359051.jpg', '1', 1, 4, 2, '2022-03-04 06:57:31', '2022-03-04 06:57:31'),
(6, 'fruit_163436567_0_1646425408.jpg', 'fruit_163436567', 1, 5, 2, '2022-03-05 01:23:28', '2022-03-05 01:23:28'),
(7, 'download_0_1646441016.jpg', 'download', 1, 6, 2, '2022-03-05 05:43:36', '2022-03-05 05:43:36'),
(8, 'pizza1_0_1646441030.jpg', 'pizza1', 1, 2, 3, '2022-03-05 05:43:50', '2022-03-05 05:43:50'),
(9, 'pizza4_0_1646698593.jpg', 'pizza4', 1, 3, 3, '2022-03-08 05:16:33', '2022-03-08 05:16:33'),
(10, 'slider1_0_1646698606.jpg', 'slider1', 1, 2, 4, '2022-03-08 05:16:46', '2022-03-08 05:16:46'),
(11, 'slider2_0_1646698618.jpg', 'slider2', 1, 1, 5, '2022-03-08 05:16:58', '2022-03-08 05:16:58');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` bigint(20) UNSIGNED NOT NULL,
  `service` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `invoice_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `sales_agent_id` int(10) UNSIGNED DEFAULT NULL,
  `discription` text COLLATE utf8mb4_unicode_ci,
  `amount` double DEFAULT NULL,
  `payment_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `payment_type` tinyint(4) NOT NULL DEFAULT '0',
  `custom_package` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `name`, `email`, `contact`, `brand`, `service`, `package`, `currency`, `client_id`, `invoice_number`, `invoice_date`, `sales_agent_id`, `discription`, `amount`, `payment_status`, `created_at`, `updated_at`, `payment_type`, `custom_package`, `transaction_id`) VALUES
(1, 'Harry Dev', 'harrydev96@gmail.com', '00000', 1, '1,2', '0', '1', 1, '2022-1', '0000-00-00', 3, 'DescriptionDescriptionDescriptionDescription', 420, 2, '2022-03-04 05:37:40', '2022-03-04 05:38:00', 1, 'asd', ''),
(2, 'test Dev', 'test@gmail.com', '123', 1, '1,2,3', '0', '2', 2, '2022-2', '0000-00-00', 1, 'asdasdasd', 111, 2, '2022-03-04 05:45:11', '2022-03-04 05:45:32', 0, 'Custom Package', ''),
(5, 'Daphne Mayo', 'daph@gmail.com', '123131', 2, '2,4', '0', '1', 4, '2022-5', '0000-00-00', 28, 'Website with marketing of it', 450, 2, '2022-03-05 01:17:26', '2022-03-05 01:18:06', 1, 'Super Combo', ''),
(6, 'test check', 'test3@gmail.com', '1000000', 2, '2,4,5', '0', '3', 5, '2022-6', '0000-00-00', 35, 'test', 4000, 2, '2022-03-05 05:38:18', '2022-03-05 05:38:41', 1, 'testtttt', ''),
(8, 'Claire Langan', 'clangan@tpi.ca', '2892317905', 3, '1', '0', '1', 8, '2022-7', '0000-00-00', 31, 'Logo Grow-up Package\r\n\r\n8 Custom Logo Design Concepts\r\nBy 3 Designers\r\nUNLIMITED Revisions\r\nStationery Design (Business Card, Letterhead, Envelope)\r\nFREE MS Word Letterhead\r\n48 to 72 hours TAT\r\nAll Final Files Format (AI, PSD, EPS, PNG, GIF, JPG, PDF)\r\n100% Satisfaction Guarantee\r\n100% Unique Design Guarantee\r\n100% Money-Back Guarantee *', 85, 2, '2022-03-08 04:22:23', '2022-03-08 04:25:45', 0, 'Logo Grow-up Package', ''),
(9, 'customer test', 'musab.technifiedlabs@gmail.com', '7777777', 1, '1,4,5,6', '0', '1', 9, '2022-8', '0000-00-00', 39, 'hello there need a full package as per discussion', 5000, 2, '2022-03-08 05:05:19', '2022-03-08 05:11:10', 1, 'supreme', ''),
(10, 'Lisandra Mendoza', 'lisandramendoza1987@gmail.com', '7868383739', 3, '1', '0', '1', 10, '2022-9', '0000-00-00', 31, NULL, 450, 2, '2022-03-09 23:54:51', '2022-03-10 00:02:52', 0, 'Abstract Logo Package', ''),
(11, 'test test', 'test321@gmail.com', '#', 3, '1', '0', '1', 11, '2022-10', '0000-00-00', 31, 'Gold Package description', 200, 1, '2022-03-26 04:38:48', '2022-03-26 04:38:48', 0, 'Gold Package', ''),
(12, 'test One', 'testoneone@gmail.com', '#', 3, '1', '0', '1', 12, '2022-11', '0000-00-00', 31, 'Description', 120, 2, '2022-03-26 06:50:56', '2022-03-26 06:51:57', 0, 'Gold Package', '');

-- --------------------------------------------------------

--
-- Table structure for table `logo_forms`
--

CREATE TABLE `logo_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo_categories` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon_based_logo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `font_style` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `additional_information` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logo_forms`
--

INSERT INTO `logo_forms` (`id`, `logo_name`, `slogan`, `business`, `logo_categories`, `icon_based_logo`, `font_style`, `additional_information`, `user_id`, `created_at`, `updated_at`, `invoice_id`, `agent_id`) VALUES
(1, 'Octavia Mckenzie', 'In quam aut facere v', 'Deserunt nulla eveni', '[\"Handmade\",\"Font including in a shape\"]', '[\"Abstract graphic\",\"Silhouet\",\"Geometric symbol\",\"Illustrated symbol\",\"Detailed illustration\",\"Mascot\",\"Seals and crests\"]', 'Sans serif', 'Quasi ab consectetur', 4, '2022-03-04 05:38:53', '2022-03-04 05:39:25', 1, 3),
(2, '', NULL, '', '', '', '', NULL, 6, '2022-03-04 05:46:04', '2022-03-04 05:46:04', 2, 1),
(4, 'Travel Inspired', 'Where your adventure begins', 'I am open to suggestions but do have the idea of a hot air balloon because it gives the impression of above and beyond and travel that is exciting and luxurious. I am very open to suggestions. I don\'t focus on beach holidays so do not want a palm tree or cruise ship.', '[\"Just font\"]', '[\"Abstract graphic\"]', 'Retro', 'Company: Travel Inspired\r\nCompany Description: Travel Agency\r\nCompany Benefits: Full service, experienced\r\nCompetitors: On line agencies.\r\nDescription Of Logo: Not sure\r\nDesign Goal: Plan a bucket list or exciting trip.\r\nFeel Of Logo: Customized planning and unique just for you type of trips. I am a very experienced agent so looking to share that and that I am trustworthy and experienced. I do specialize in Europe and my ideal clients are typically 50+ women.\r\nColour Scheme: I really like orange but know that is a difficult colour. I like it because it has a bit of a retro feel. I also like all blues and greens. I would like it to have a feminine feel but not girly pink.\r\nAudience Description: My target audience is 50+ women who travel with both their partner and with their girlfriends. Well travelled and willing to pay for comfort and luxury.\r\nGender: Female\r\nAge Range: 50-75\r\nOther Audience Description: Bucket list travellers planning extensive trips to Europe and Australia.\r\nAdditional Information: I can\'t think of anything just now.', 38, '2022-03-08 04:28:28', '2022-03-08 04:32:23', 8, 31),
(5, 'Ella Rivas', 'Magnam nostrud quibu', 'Architecto ut et adi', '[\"Font + meaning\",\"Font including in a shape\"]', '[\"Abstract graphic\",\"Illustrated symbol\",\"Detailed illustration\",\"Mascot\"]', 'Retro', 'Cillum et ut omnis h', 41, '2022-03-08 05:14:41', '2022-03-08 05:16:18', 9, 39),
(6, 'Avatar CBC', NULL, '', 'null', 'null', '', NULL, 44, '2022-03-10 00:04:09', '2022-03-11 03:55:18', 10, 31),
(7, 'My Logo Test One', NULL, '', 'null', 'null', '', NULL, 46, '2022-03-26 06:53:10', '2022-03-26 06:54:51', 12, 31);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `task_id` bigint(20) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `message`, `sender_id`, `created_at`, `updated_at`, `task_id`, `role_id`) VALUES
(1, 41, 'Hello World Testing', 41, '2022-03-16 06:44:00', '2022-03-16 06:44:00', NULL, 3),
(2, 44, 'HI', 44, '2022-03-16 06:46:30', '2022-03-16 06:46:30', NULL, 3),
(3, 44, 'Hy Agent..\r\nClient Here', 44, '2022-03-18 05:45:21', '2022-03-18 05:45:21', NULL, 3),
(4, 44, 'Hi', 44, '2022-03-18 06:34:52', '2022-03-18 06:34:52', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_04_26_085235_add_is_employee_in_users_table', 1),
(5, '2021_04_30_053839_add_paid_to_users_table', 1),
(6, '2021_04_30_063838_add_seller__registration__colums_to_users', 1),
(7, '2021_04_30_222006_add_status_and_block_to_users_table', 1),
(8, '2021_05_15_085430_create_categories_table', 1),
(9, '2021_05_15_114723_create_brands_table', 1),
(10, '2021_05_17_021212_removing_extra_cols_from_users_table', 1),
(11, '2021_05_17_021442_add_brand_id_to_users_table', 1),
(12, '2021_05_17_050739_create_category_users_table', 1),
(13, '2021_05_17_221242_create_projects_table', 1),
(14, '2021_05_17_224857_create_clients_table', 1),
(15, '2021_05_18_231609_add_user_id_to_clients_table', 1),
(16, '2021_05_18_235551_add_status_id_to_clients_table', 1),
(17, '2021_05_30_023444_add_cost_to_projects_table', 1),
(18, '2021_05_30_023757_add_client_id_to_projects_table', 1),
(19, '2021_06_02_153024_create_category_project_table', 1),
(20, '2021_06_02_154432_add_updated_at_to_category_project_table', 1),
(21, '2021_06_02_155205_add_brand_id_to_projects_table', 1),
(22, '2021_06_02_174646_create_tasks_table', 1),
(23, '2021_06_02_175212_add_status_to_tasks_table', 1),
(25, '2021_06_03_143047_add_user_id_to_tasks_table', 1),
(26, '2021_06_04_150205_add_brand_id_to_tasks_table', 1),
(27, '2021_06_04_175834_create_sub_task_table', 1),
(28, '2021_06_04_181801_add_user_id_to_sub_task_table', 1),
(29, '2021_07_10_044235_add_logo_and_auth_key_to_brands_table', 1),
(30, '2021_07_10_052315_add_phone_and_email_and_address_to_brands_table', 1),
(31, '2021_07_10_061447_add_image_to_users_table', 1),
(32, '2021_07_12_134410_add_sign_to_brands_table', 1),
(33, '2021_07_12_144124_create_services_table', 1),
(34, '2021_07_12_194309_add_brand_id_to_clients_table', 1),
(35, '2021_07_12_222433_add_url_and_subject_and_services_and_message_to_clients_table', 1),
(36, '2021_07_12_230717_remove_unique_from_email_to_clients_table', 1),
(37, '2021_09_26_014422_create_brand_users_table', 1),
(38, '2021_09_26_053725_add_duedate_to_tasks_table', 1),
(39, '2021_09_26_062003_add_duedate_to_sub_task_table', 1),
(41, '2021_10_03_044823_create_packages_table', 1),
(42, '2021_10_03_055711_add_paid_status_packages_table', 1),
(43, '2021_10_03_062735_create_currencies_table', 1),
(44, '2021_10_03_065724_add_sign_to_packages_table', 1),
(45, '2021_10_04_221625_create_notifications_table', 1),
(46, '2021_10_10_045955_create_invoices_table', 1),
(47, '2021_10_10_054657_add_payment_type_to_invoices_table', 1),
(48, '2021_10_17_065317_add_custom_package_name_to_invoice_table', 1),
(49, '2021_10_17_083112_add_customer_stripe_to_invoice_table', 1),
(50, '2021_10_19_041420_add_transaction_id_to_invoices_table', 1),
(51, '2021_11_14_090646_create_roles_table', 1),
(52, '2021_11_14_091258_add_role_id_to_role_table', 1),
(53, '2021_11_14_092153_change_contact_nullable_to_clients_table', 1),
(54, '2021_11_14_100803_add_client_id_to_users_table', 1),
(55, '2021_11_14_113442_create_messages_table', 1),
(56, '2021_11_15_073747_add_assign_id_to_clients_table', 1),
(57, '2021_11_17_060848_create_logo_forms_table', 1),
(58, '2021_11_17_062331_add_form_to_services_table', 1),
(59, '2021_11_17_071631_add_invoice_id_to_logo_forms_table', 1),
(60, '2021_11_18_224522_create_form_files_table', 1),
(61, '2021_11_19_042252_create_web_forms_table', 1),
(62, '2021_11_21_104402_add_agent_id_to_logo_forms_table', 1),
(63, '2021_11_21_105635_add_agent_id_to_web_forms_table', 1),
(64, '2021_11_25_054339_add_form_id_to_projects_table', 1),
(65, '2022_02_18_043224_add_show_to_client_table', 1),
(67, '2022_02_22_230845_create_smm_forms_table', 1),
(68, '2022_02_22_235626_add_agent_id_to_users_table', 1),
(69, '2022_02_23_001005_create_content_writing_forms_table', 1),
(70, '2022_02_23_002300_create_seo_forms_table', 1),
(71, '2022_02_23_054710_add_company_name_to_seo_forms_table', 1),
(72, '2022_02_24_232047_add_assign_id_to_sub_task_table', 1),
(73, '2022_02_25_012213_add_status_to_sub_task_table', 1),
(75, '2022_02_25_053710_add_production_check_to_sub_task_table', 1),
(76, '2022_02_26_005155_add_notes_to_tasks_table', 1),
(83, '2021_06_03_125327_create_client_files_table', 2),
(84, '2021_09_27_050444_add_user_id_status_to_client_files_table', 2),
(85, '2022_02_19_042415_add_task_id_to_messages_table', 2),
(86, '2022_02_25_041300_add_production_check_to_client_files_table', 2),
(87, '2022_02_26_035651_add_message_id_to_client_files_table', 2),
(88, '2022_03_13_060339_add_role_id_to_users_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('0442c66b-e514-466e-96f4-b79c6b5bbe27', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 45, '{\"id\":44,\"task_id\":\"4\",\"project_id\":8,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hi Danny\",\"url\":\"\"}', NULL, '2022-03-11 04:17:57', '2022-03-11 04:17:57'),
('0e13d368-6046-426a-8ad3-84f755fbde29', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":27,\"task_id\":\"1\",\"project_id\":2,\"name\":\"Steve Stark\",\"text\":\"Steve Stark has send you a Message\",\"details\":\"hello\",\"url\":\"\"}', NULL, '2022-03-05 01:44:35', '2022-03-05 01:44:35'),
('124021c9-aa16-49c5-8abf-8355cf86b6b4', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 40, '{\"id\":41,\"name\":\"customer testa\",\"text\":\"customer testa has send you a Message\",\"details\":\"Hello World Testing\",\"url\":\"\"}', NULL, '2022-03-16 06:44:00', '2022-03-16 06:44:00'),
('179aa6b4-c848-49fa-8566-a49e1cb9d762', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 36, '{\"id\":37,\"task_id\":\"2\",\"project_id\":5,\"name\":\"test check\",\"text\":\"test check has send you a Message\",\"details\":\"hi\",\"url\":\"\"}', NULL, '2022-03-05 05:57:09', '2022-03-05 05:57:09'),
('1d674227-f74b-4e33-b198-ded598dda2ea', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 36, '{\"id\":37,\"task_id\":\"2\",\"project_id\":5,\"name\":\"test check\",\"text\":\"test check has send you a Message\",\"details\":\"check it\",\"url\":\"\"}', NULL, '2022-03-05 05:57:22', '2022-03-05 05:57:22'),
('1d889a46-8a64-400c-a610-fdb3f40d6a17', 'App\\Notifications\\TaskNotification', 'App\\Models\\User', 1, '{\"task_id\":3,\"name\":\"Task Assigned by support agent (Logo Design)\",\"text\":\"Task Assigned by support agent (Logo Design)\",\"details\":\"test\"}', NULL, '2022-03-08 05:28:58', '2022-03-08 05:28:58'),
('1f9ed939-3448-4b77-8be8-6ba574dcbebc', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 41, '{\"id\":40,\"task_id\":\"3\",\"project_id\":6,\"name\":\"support agent\",\"text\":\"support agent has send you a Message\",\"details\":\"check this one\",\"url\":\"\"}', '2022-03-08 05:30:16', '2022-03-08 05:29:25', '2022-03-08 05:30:16'),
('2d4fe82a-fe6f-44e4-9daa-0d18bf76ff44', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":36,\"task_id\":\"2\",\"project_id\":5,\"name\":\"john andrew\",\"text\":\"john andrew has send you a Message\",\"details\":\"hey\\r\\nasd\",\"url\":\"\"}', NULL, '2022-03-05 05:54:47', '2022-03-05 05:54:47'),
('30375278-4d09-4ad4-b30f-f317e2530082', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":36,\"task_id\":\"2\",\"project_id\":5,\"name\":\"john andrew\",\"text\":\"john andrew has send you a Message\",\"details\":\"test\",\"url\":\"\"}', NULL, '2022-03-05 05:58:04', '2022-03-05 05:58:04'),
('380628f5-a9d8-4eaf-bfe7-be2a5b3df6e7', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":37,\"task_id\":\"2\",\"project_id\":5,\"name\":\"test check\",\"text\":\"test check has send you a Message\",\"details\":\"check it\",\"url\":\"\"}', NULL, '2022-03-05 05:57:22', '2022-03-05 05:57:22'),
('38e47456-a6d6-4533-a454-cd7395b9cf2d', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hi\",\"url\":\"\"}', NULL, '2022-03-18 06:34:52', '2022-03-18 06:34:52'),
('43daa60f-0a1c-4303-b5d7-071e44058a31', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 36, '{\"id\":1,\"project_id\":4,\"name\":\"Admin \",\"text\":\"Ortiz and Long Inc - CONTENT WRITING has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-05 05:45:37', '2022-03-05 05:45:37'),
('49e94e56-d831-4539-9cf2-4b2aa59ff84b', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 45, '{\"id\":1,\"project_id\":9,\"name\":\"Admin \",\"text\":\"My Logo Test One - LOGO has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-26 06:55:48', '2022-03-26 06:55:48'),
('59080862-3c8d-4172-9c1f-d3dbf0af64a1', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":40,\"task_id\":\"3\",\"project_id\":6,\"name\":\"support agent\",\"text\":\"support agent has send you a Message\",\"details\":\"check this one\",\"url\":\"\"}', NULL, '2022-03-08 05:29:25', '2022-03-08 05:29:25'),
('5aecab2f-1bbb-4b83-954b-3406868e2f26', 'App\\Notifications\\TaskNotification', 'App\\Models\\User', 42, '{\"task_id\":3,\"name\":\"Task Assigned by support agent\",\"text\":\"Task Assigned by support agent (Logo Design)\",\"details\":\"check this one out now\"}', NULL, '2022-03-08 05:42:47', '2022-03-08 05:42:47'),
('5f148dce-1837-49a4-970e-0a54d5f2ea3e', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 39, '{\"id\":41,\"name\":\"customer testa\",\"text\":\"customer testa has send you a Message\",\"details\":\"Hello World Testing\",\"url\":\"\"}', NULL, '2022-03-16 06:44:00', '2022-03-16 06:44:00'),
('643582f6-2a19-4ed6-8f3a-858bbd46cbe7', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 9, '{\"id\":1,\"project_id\":1,\"name\":\"Admin \",\"text\":\"Octavia Mckenzie - LOGO has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-04 06:07:27', '2022-03-04 06:07:27'),
('691c19c4-62a8-4d56-a4ce-84e1caeb8320', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 45, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"HI\",\"url\":\"\"}', NULL, '2022-03-16 06:46:31', '2022-03-16 06:46:31'),
('6dda1c37-6890-4e1d-afd7-ea3e2c1d8113', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":41,\"task_id\":\"3\",\"project_id\":6,\"name\":\"customer testa\",\"text\":\"customer testa has send you a Message\",\"details\":\"hey\",\"url\":\"\"}', NULL, '2022-03-08 05:30:23', '2022-03-08 05:30:23'),
('77e91e1f-e9bf-46d4-ab06-51f4f05818be', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":37,\"task_id\":\"2\",\"project_id\":5,\"name\":\"test check\",\"text\":\"test check has send you a Message\",\"details\":\"hi\",\"url\":\"\"}', NULL, '2022-03-05 05:57:09', '2022-03-05 05:57:09'),
('8413c40f-d652-4572-8973-dfe3e1deaac8', 'App\\Notifications\\TaskNotification', 'App\\Models\\User', 1, '{\"task_id\":3,\"name\":\"Task Assigned by support agent (Logo Design)\",\"text\":\"Task Assigned by support agent (Logo Design)\",\"details\":\"check this one out now\"}', NULL, '2022-03-08 05:42:47', '2022-03-08 05:42:47'),
('8aa03e00-bcbc-4584-b697-59b9e4a2e4f6', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"HI\",\"url\":\"\"}', NULL, '2022-03-16 06:46:31', '2022-03-16 06:46:31'),
('8c05ca57-2af7-4211-a011-8d0a7e8f4ed9', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 41, '{\"id\":40,\"task_id\":\"3\",\"project_id\":6,\"name\":\"support agent\",\"text\":\"support agent has send you a Message\",\"details\":\"hey\",\"url\":\"\"}', '2022-03-08 05:44:18', '2022-03-08 05:29:09', '2022-03-08 05:44:18'),
('8cfff858-b548-4f7e-910c-ea37c9a82b96', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 40, '{\"id\":1,\"project_id\":7,\"name\":\"Admin \",\"text\":\"Cohen Rasmussen Traders - SMM has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-08 05:25:52', '2022-03-08 05:25:52'),
('900027b0-4a4d-47f6-b55a-1acf202c1842', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 36, '{\"id\":1,\"project_id\":3,\"name\":\"Admin \",\"text\":\"Hayes Chavez Traders - SMM has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-05 05:45:23', '2022-03-05 05:45:23'),
('921a7a84-6514-4d3b-a747-4a2d92d77eb2', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 29, '{\"id\":27,\"task_id\":\"1\",\"project_id\":2,\"name\":\"Steve Stark\",\"text\":\"Steve Stark has send you a Message\",\"details\":\"im good\",\"url\":\"\"}', NULL, '2022-03-05 01:45:54', '2022-03-05 01:45:54'),
('929a6c33-a7fb-4466-b644-b85c8fb12c6e', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 31, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"HI\",\"url\":\"\"}', NULL, '2022-03-16 06:46:30', '2022-03-16 06:46:30'),
('9969462e-70e1-48cb-9440-5bee8ca3f1a6', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":44,\"task_id\":\"4\",\"project_id\":8,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hi Danny\",\"url\":\"\"}', NULL, '2022-03-11 04:17:57', '2022-03-11 04:17:57'),
('99a86e6e-1399-4035-b78a-6e64c08d9715', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 45, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hy Agent..\\r\\nClient Here\",\"url\":\"\"}', NULL, '2022-03-18 05:45:22', '2022-03-18 05:45:22'),
('9b75c884-c3be-454b-8294-ddbecbec603d', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":27,\"task_id\":\"1\",\"project_id\":2,\"name\":\"Steve Stark\",\"text\":\"Steve Stark has send you a Message\",\"details\":\"im good\",\"url\":\"\"}', NULL, '2022-03-05 01:45:54', '2022-03-05 01:45:54'),
('9cd20f5b-c8c3-4ff1-ba38-ac6203194eba', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 31, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hy Agent..\\r\\nClient Here\",\"url\":\"\"}', NULL, '2022-03-18 05:45:22', '2022-03-18 05:45:22'),
('9de04be0-c31f-414d-97b8-8e84d6977c24', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 37, '{\"id\":36,\"task_id\":\"2\",\"project_id\":5,\"name\":\"john andrew\",\"text\":\"john andrew has send you a Message\",\"details\":\"test\",\"url\":\"\"}', NULL, '2022-03-05 05:58:04', '2022-03-05 05:58:04'),
('af4833b7-e076-4112-b215-6d194bfa1950', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":41,\"name\":\"customer testa\",\"text\":\"customer testa has send you a Message\",\"details\":\"Hello World Testing\",\"url\":\"\"}', NULL, '2022-03-16 06:44:00', '2022-03-16 06:44:00'),
('b0efb593-51a8-49d3-9566-c66934bae33b', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 36, '{\"id\":1,\"project_id\":5,\"name\":\"Admin \",\"text\":\"Byron Butler - WEBSITE has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-05 05:47:41', '2022-03-05 05:47:41'),
('b17a8e1f-894d-48b6-9a8c-b9a7879666b4', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 40, '{\"id\":1,\"project_id\":6,\"name\":\"Admin \",\"text\":\"Ella Rivas - LOGO has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-08 05:24:53', '2022-03-08 05:24:53'),
('b452a082-1f75-4e02-aa30-6e60635a84df', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 29, '{\"id\":27,\"task_id\":\"1\",\"project_id\":2,\"name\":\"Steve Stark\",\"text\":\"Steve Stark has send you a Message\",\"details\":\"hello\",\"url\":\"\"}', NULL, '2022-03-05 01:44:35', '2022-03-05 01:44:35'),
('bdddd9f2-ecab-41fa-a778-3537c160b154', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 45, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hi\",\"url\":\"\"}', NULL, '2022-03-18 06:34:52', '2022-03-18 06:34:52'),
('c2b15db0-741e-4a86-9a4a-e84b851b2f42', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 31, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hi\",\"url\":\"\"}', NULL, '2022-03-18 06:34:52', '2022-03-18 06:34:52'),
('d2e78628-4f54-4579-a64b-0a24bd723912', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 40, '{\"id\":41,\"task_id\":\"3\",\"project_id\":6,\"name\":\"customer testa\",\"text\":\"customer testa has send you a Message\",\"details\":\"hey\",\"url\":\"\"}', NULL, '2022-03-08 05:30:23', '2022-03-08 05:30:23'),
('df2c6f89-d044-46af-8dba-7c65d7e1d3fe', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 37, '{\"id\":36,\"task_id\":\"2\",\"project_id\":5,\"name\":\"john andrew\",\"text\":\"john andrew has send you a Message\",\"details\":\"hey\\r\\nasd\",\"url\":\"\"}', '2022-03-05 05:57:04', '2022-03-05 05:54:47', '2022-03-05 05:57:04'),
('df88619e-d497-4ce3-96aa-5029c5ddf925', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":40,\"task_id\":\"3\",\"project_id\":6,\"name\":\"support agent\",\"text\":\"support agent has send you a Message\",\"details\":\"hey\",\"url\":\"\"}', NULL, '2022-03-08 05:29:09', '2022-03-08 05:29:09'),
('e019e972-38bb-400f-84cf-fa511325f701', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 27, '{\"id\":1,\"project_id\":2,\"name\":\"Admin \",\"text\":\"Briar Arnold - WEBSITE has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-05 01:25:55', '2022-03-05 01:25:55'),
('eb96c947-9a5e-4a25-a215-b60ae0601ccc', 'App\\Notifications\\AssignProjectNotification', 'App\\Models\\User', 45, '{\"id\":1,\"project_id\":8,\"name\":\"Admin \",\"text\":\"Avatar CBC - LOGO has assign. (Admin)\",\"url\":\"\"}', NULL, '2022-03-11 03:58:41', '2022-03-11 03:58:41'),
('f157e4c3-754d-40f0-b3c8-5433dcba84fe', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":44,\"name\":\"Lisandra Mendoza\",\"text\":\"Lisandra Mendoza has send you a Message\",\"details\":\"Hy Agent..\\r\\nClient Here\",\"url\":\"\"}', NULL, '2022-03-18 05:45:22', '2022-03-18 05:45:22'),
('f3837861-83ae-4075-8bed-5d3bb7d435fe', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 1, '{\"id\":45,\"task_id\":\"4\",\"project_id\":8,\"name\":\"Lucas Scott\",\"text\":\"Lucas Scott has send you a Message\",\"details\":\"Hi\",\"url\":\"\"}', NULL, '2022-03-11 04:22:14', '2022-03-11 04:22:14'),
('f6652330-94e7-437c-9391-f12f25c79f97', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 40, '{\"id\":41,\"name\":\"customer testa\",\"text\":\"customer testa has send you a Message\",\"details\":\"Hello World Testing\",\"url\":\"\"}', NULL, '2022-03-16 06:44:00', '2022-03-16 06:44:00'),
('ffe24a6a-c6df-4a21-be51-09ae794197ef', 'App\\Notifications\\MessageNotification', 'App\\Models\\User', 44, '{\"id\":45,\"task_id\":\"4\",\"project_id\":8,\"name\":\"Lucas Scott\",\"text\":\"Lucas Scott has send you a Message\",\"details\":\"Hi\",\"url\":\"\"}', '2022-03-19 08:32:39', '2022-03-11 04:22:14', '2022-03-19 08:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actual_price` decimal(65,2) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cut_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `addon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `best_selling` tinyint(4) NOT NULL DEFAULT '0',
  `on_landing` tinyint(4) NOT NULL DEFAULT '0',
  `is_combo` tinyint(4) NOT NULL DEFAULT '0',
  `brand_id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `currencies_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `product_status` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cost` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `form_id` bigint(20) UNSIGNED DEFAULT NULL,
  `form_checker` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `status`, `product_status`, `user_id`, `created_at`, `updated_at`, `cost`, `client_id`, `brand_id`, `form_id`, `form_checker`) VALUES
(1, 'Octavia Mckenzie - LOGO', 'Deserunt nulla eveni', 1, 0, 9, '2022-03-04 06:07:27', '2022-03-04 06:07:27', NULL, 4, 1, 1, 1),
(2, 'Briar Arnold - WEBSITE', NULL, 1, 0, 27, '2022-03-05 01:25:55', '2022-03-05 01:25:55', NULL, 29, 2, 5, 2),
(3, 'Hayes Chavez Traders - SMM', 'Non eum cupiditate p', 1, 0, 36, '2022-03-05 05:45:23', '2022-03-05 05:45:23', NULL, 37, 2, 2, 3),
(4, 'Ortiz and Long Inc - CONTENT WRITING', 'Unde incididunt sunt', 1, 0, 36, '2022-03-05 05:45:37', '2022-03-05 05:45:37', NULL, 37, 2, 1, 4),
(5, 'Byron Butler - WEBSITE', NULL, 1, 0, 36, '2022-03-05 05:47:41', '2022-03-05 05:47:41', NULL, 37, 2, 6, 2),
(6, 'Ella Rivas - LOGO', 'Architecto ut et adi', 1, 0, 40, '2022-03-08 05:24:53', '2022-03-08 05:24:53', NULL, 41, 1, 5, 1),
(7, 'Cohen Rasmussen Traders - SMM', 'Ad aut consectetur', 1, 0, 40, '2022-03-08 05:25:52', '2022-03-08 05:25:52', NULL, 41, 1, 3, 3),
(8, 'Avatar CBC - LOGO', '', 1, 0, 45, '2022-03-11 03:58:41', '2022-03-11 03:58:41', NULL, 44, 2, 6, 1),
(9, 'My Logo Test One - LOGO', '', 1, 0, 45, '2022-03-26 06:55:48', '2022-03-26 06:55:48', NULL, 46, 3, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `column_name`, `role_name`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'is_employee', 'Sale Agent', NULL, NULL, 0),
(2, 'is_employee', 'Production Team Lead', NULL, NULL, 1),
(3, 'is_employee', 'Admin', NULL, NULL, 2),
(4, 'is_employee', 'Customer', NULL, NULL, 3),
(5, 'is_employee', 'Customer Support', NULL, NULL, 4),
(6, 'is_employee', 'Production Member', NULL, NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `seo_forms`
--

CREATE TABLE `seo_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_established` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_current_site` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `top_goals` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `core_offer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `average_order_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `selling_per_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_lifetime_value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supplementary_offers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `getting_clients` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currently_spending` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_visitors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `people_adding` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_financial` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `that_much` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specific_target` text COLLATE utf8mb4_unicode_ci,
  `competitors` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_party_marketing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_monthly_sales` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_monthly_revenue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `looking_to_execute` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seo_forms`
--

INSERT INTO `seo_forms` (`id`, `business_established`, `original_owner`, `age_current_site`, `top_goals`, `core_offer`, `average_order_value`, `selling_per_month`, `client_lifetime_value`, `supplementary_offers`, `getting_clients`, `currently_spending`, `monthly_visitors`, `people_adding`, `monthly_financial`, `that_much`, `specific_target`, `competitors`, `third_party_marketing`, `current_monthly_sales`, `current_monthly_revenue`, `target_region`, `looking_to_execute`, `time_zone`, `user_id`, `invoice_id`, `agent_id`, `created_at`, `updated_at`, `company_name`) VALUES
(1, '1977', 'Ipsa fugiat suscipi', 'Pariatur Facere nes', 'Non nostrud dolores', 'Proident doloremque', 'Laudantium quo veli', 'Veritatis iure lauda', 'Dolore mollit quos a', 'Est aut aliquam eos', 'Voluptatem obcaecati', 'Nulla quo aspernatur', 'Itaque atque unde su', 'Tempore unde ipsum', 'Provident asperiore', 'Veniam ratione sunt', 'Ut natus non iure qu', 'Nisi cupidatat ratio', 'Asperiores maxime co', 'Vel eos quo necessit', 'Culpa labore illo o', 'Deserunt autem nulla', 'Sint sit tempor dol', 'Dolorum est deserunt', 41, 9, 39, '2022-03-08 05:14:42', '2022-03-08 05:16:58', 'Benton House Inc');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `form` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `brand_id`, `created_at`, `updated_at`, `form`) VALUES
(1, 'Logo Design', 1, '2022-03-04 05:33:07', '2022-03-04 05:33:07', 1),
(2, 'Website Design', 1, '2022-03-04 05:33:17', '2022-03-04 05:33:17', 2),
(3, 'E-Commerce Solution', 1, '2022-03-04 05:33:24', '2022-03-04 05:33:24', 2),
(4, 'Social Media Marketing', 1, '2022-03-04 05:33:33', '2022-03-04 05:33:33', 3),
(5, 'Content Writing', 1, '2022-03-04 05:33:46', '2022-03-04 05:33:46', 4),
(6, 'Search Engine Optimization', 1, '2022-03-04 05:33:53', '2022-03-04 05:33:53', 5);

-- --------------------------------------------------------

--
-- Table structure for table `smm_forms`
--

CREATE TABLE `smm_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `desired_results` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_mailing_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_website_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_working_hours` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `business_category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_media_platforms` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_locations` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target_audience` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_bracket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `represent_your_business` text COLLATE utf8mb4_unicode_ci,
  `business_usp` text COLLATE utf8mb4_unicode_ci,
  `do_not_want_us_to_use` text COLLATE utf8mb4_unicode_ci,
  `competitors` text COLLATE utf8mb4_unicode_ci,
  `additional_comments` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smm_forms`
--

INSERT INTO `smm_forms` (`id`, `desired_results`, `business_name`, `business_email_address`, `business_phone_number`, `business_mailing_address`, `business_location`, `business_website_address`, `business_working_hours`, `business_category`, `social_media_platforms`, `target_locations`, `target_audience`, `age_bracket`, `represent_your_business`, `business_usp`, `do_not_want_us_to_use`, `competitors`, `additional_comments`, `user_id`, `invoice_id`, `created_at`, `updated_at`, `agent_id`) VALUES
(1, '[\"Increase in Page Likes\\/Followers\",\"Social Media Management\"]', 'Rowe and Brennan Traders', 'peqa@mailinator.com', '+1 (903) 506-6082', 'wucewi@mailinator.com', 'Mollit cupidatat occ', 'https://www.cisimasic.org.au', 'Nisi nulla earum ut', 'Non inventore laudan', '[\"YouTube\",\"Linkedin\"]', 'Molestiae est ut eve', '[\"Female\"]', 'Aperiam explicabo Q', 'At et in dicta Nam e', 'Est veniam reprehen', 'In molestiae aut deb', 'Voluptatem reiciend', 'Quisquam dolor volup', 29, 5, '2022-03-05 01:19:03', '2022-03-05 01:26:28', 28),
(2, '[\"Increase in Page Likes\\/Followers\",\"Social Media Management\",\"Brand Awareness\"]', 'Hayes Chavez Traders', 'xakexap@mailinator.com', '+1 (326) 715-9626', 'sanizapi@mailinator.com', 'Placeat velit ipsa', 'https://www.tykywex.cm', 'Dolore dolor delectu', 'Non eum cupiditate p', '[\"Twitter\",\"YouTube\",\"Linkedin\"]', 'Recusandae Quam sae', '[\"Male\",\"Female\"]', 'Suscipit sequi sit', 'Odit vel cumque iust', 'Magna eius unde dolo', 'Qui sunt ut quia exc', 'Eiusmod unde dolores', 'Aliqua Est nostrud', 37, 6, '2022-03-05 05:40:22', '2022-03-05 05:43:50', 35),
(3, '[\"Social Media Management\"]', 'Cohen Rasmussen Traders', 'wapusygula@mailinator.com', '+1 (554) 571-7906', 'sujaxofi@mailinator.com', 'Magni nulla tempor i', 'https://www.gakenyrec.cm', 'Aut enim ullam dolor', 'Ad aut consectetur', '[\"Facebook\",\"Twitter\",\"Pinterest\"]', 'Et omnis sapiente qu', '[\"Male\",\"Female\",\"Both\"]', 'Non eaque aspernatur', 'Neque sit Nam minim', 'Maiores nisi quisqua', 'In qui tempore magn', 'Tempor veniam paria', 'Est quam saepe moles', 41, 9, '2022-03-08 05:14:41', '2022-03-08 05:16:33', 39);

-- --------------------------------------------------------

--
-- Table structure for table `sub_task`
--

CREATE TABLE `sub_task` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `task_id` bigint(20) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `duedate` date DEFAULT NULL,
  `assign_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `sub_task_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_task`
--

INSERT INTO `sub_task` (`id`, `task_id`, `description`, `created_at`, `updated_at`, `user_id`, `duedate`, `assign_id`, `status`, `sub_task_id`) VALUES
(1, 1, '<p>Need Designs</p>', '2022-03-05 01:40:06', '2022-03-05 01:40:06', 27, '2022-03-24', NULL, 0, 0),
(2, 2, '<p>testttttt</p>', '2022-03-05 05:54:32', '2022-03-05 05:54:32', 36, '2022-04-01', NULL, 0, 0),
(3, 3, '<p>test</p>', '2022-03-08 05:28:58', '2022-03-08 05:37:32', 40, '2022-03-26', 43, 0, 0),
(4, 3, '<p>abc</p>', '2022-03-08 05:40:14', '2022-03-08 05:40:14', 43, NULL, NULL, 0, 3),
(5, 3, '<p>production task</p>', '2022-03-08 05:41:10', '2022-03-08 05:41:10', 42, NULL, NULL, 0, 0),
(6, 3, '<p>check this one out now</p>', '2022-03-08 05:42:47', '2022-03-08 05:49:03', 40, '2022-03-24', 43, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `project_id` bigint(20) UNSIGNED DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `duedate` date NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `project_id`, `category_id`, `description`, `created_at`, `updated_at`, `status`, `user_id`, `brand_id`, `duedate`, `notes`) VALUES
(1, 2, 2, '<p>Need Frontend of the website</p>', '2022-03-05 01:39:02', '2022-03-05 01:43:02', 1, 27, 2, '2022-03-17', 'test asd asdasdzxczxc'),
(2, 5, 2, '<p>Complete front end of web</p>', '2022-03-05 05:53:06', '2022-03-05 05:54:32', 1, 36, 2, '2022-03-24', NULL),
(3, 6, 1, '<p>make logo on apple</p>', '2022-03-08 05:27:01', '2022-03-08 05:42:47', 1, 40, 1, '2022-03-30', 'asda sd'),
(4, 8, 1, '<p>Need Final files</p>', '2022-03-11 04:16:17', '2022-03-11 04:16:17', 0, 45, 2, '2022-03-11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_employee` tinyint(1) DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `is_employee`, `last_name`, `contact`, `status`, `block`, `brand_id`, `image`, `client_id`) VALUES
(1, 'Admin', 'admin@designcrm.net', NULL, '$2y$10$i.qx/A4eUHjlNVsbA/IQh.4j4tBjDNDAAMnW2C6BWWYT95HciTmGm', NULL, NULL, NULL, 2, NULL, NULL, 1, 0, NULL, NULL, 0),
(2, 'Paul', 'paul@gmail.com', NULL, '$2y$10$8d.VYJr.qj78QujYfBENuOMzt1RbONdQ4ficuEX.SvgCX5d1Kb8Li', NULL, '2022-03-04 05:35:35', '2022-03-04 05:35:35', 4, 'White', '123132', 1, 0, NULL, NULL, 0),
(3, 'Oliver', 'oliver@gmail.com', NULL, '$2y$10$JPkXw6eKcIHvdXifDvixge2kC/3gp1K9aMRwdWjwQZefkReMV4b6m', NULL, '2022-03-04 05:36:07', '2022-03-04 05:36:07', 0, 'Smith', '2222222', 1, 0, NULL, NULL, 0),
(4, 'Harry', 'harrydev96@gmail.com', NULL, '$2y$10$K6zHjlDyc9DSW5CwLqUVB.SClfCQTo8ZE7UmIT2X3ALFniD64y.eK', NULL, '2022-03-04 05:38:53', '2022-03-04 05:38:53', 3, 'Dev', '00000', 1, 0, NULL, NULL, 1),
(5, 'Martin', 'martin.gilbert@designsgene.com', NULL, '$2y$10$2gu7IlAd.6z5sosiJE0IJOEh8zZ56d.r7xouV0CswWPjfXo4dFkmm', NULL, '2022-03-04 05:46:03', '2022-03-04 05:46:03', 0, 'Gilbert', NULL, 1, 0, NULL, NULL, 0),
(6, 'test', 'test@gmail.com', NULL, '$2y$10$UNVjXsVHOJx1fpZApzuyk.OG442zXBpfwY4HkYoMWkFXHxBxlEFaC', NULL, '2022-03-04 05:46:04', '2022-03-04 05:46:04', 3, 'Dev', '123', 1, 0, NULL, NULL, 2),
(7, 'Allen', 'allen.maxwell@designsgene.com', NULL, '$2y$10$eG5jRJ0Yjde12sHMgiFqwuzWKqdOl4hLTekk1KBbh0943b8sgKS0W', NULL, '2022-03-04 05:46:57', '2022-03-04 05:46:57', 0, 'Maxwell', NULL, 1, 0, NULL, NULL, 0),
(8, 'Leonard', 'leonard.anderson@designsgene.com', NULL, '$2y$10$vCnmQW/or3zBufU3Np3GTuwQNY.zswlzD1h5jaaoDlfLxyX4hqaEu', NULL, '2022-03-04 05:47:45', '2022-03-04 05:47:45', 0, 'Anderson', NULL, 1, 0, NULL, NULL, 0),
(9, 'Ben', 'ben@designsgene.com', NULL, '$2y$10$ADTtNbrNd9j6zfPibVoPnOr0wJzvWN4NP4rsRNd/1heIJFR7weZ8a', NULL, '2022-03-04 05:48:21', '2022-03-04 05:48:21', 4, 'Mark', NULL, 1, 0, NULL, NULL, 0),
(10, 'Christoper', 'christopher.prez@designscottage.com', NULL, '$2y$10$BGo/7c6.IDkLav7OUVeAzuXd2JT7aibYcuquUS9jx0ZMESdfkohmi', NULL, '2022-03-04 05:49:16', '2022-03-04 05:49:16', 4, 'Prez', NULL, 1, 0, NULL, NULL, 0),
(11, 'Fiona', 'fiona.becker@designscottage.com', NULL, '$2y$10$6kJpGrsHqSnFgUwxXP41heSz2Evwd.VHAPCxUPiUyf0Axc.C1x5kG', NULL, '2022-03-04 05:50:36', '2022-03-04 05:50:36', 4, 'Becker', NULL, 1, 0, NULL, NULL, 0),
(12, 'Lily', 'lily.ann@designsgene.com', NULL, '$2y$10$2LQlOLK6fFJtED.kMQ7yaOW4olti8FHIupH4g/YJ9k9WGd12zWb/q', NULL, '2022-03-04 05:51:17', '2022-03-04 05:51:17', 4, 'Ann', NULL, 1, 0, NULL, NULL, 0),
(13, 'Josh', 'josh.abraham@designsgene.com', NULL, '$2y$10$ouLB3yviOw5G7qyqzU.jz.pRysI/F7.1wBJaRn7CR5iTNPyK.PX5K', NULL, '2022-03-04 05:51:57', '2022-03-04 05:51:57', 4, 'Abraham', NULL, 1, 0, NULL, NULL, 0),
(14, 'Calvin', 'calvin.scofield@designsgene.com', NULL, '$2y$10$dUdYrbgwhH2lUm3v5wo1RO5WCy8BiDWp.ydkuJtquRljVz6X8YsL.', NULL, '2022-03-04 05:53:07', '2022-03-04 05:53:07', 0, 'Scofield', NULL, 1, 0, NULL, NULL, 0),
(15, 'Alvin', 'alvin.scofield@designsgene.com', NULL, '$2y$10$fB9Ui6RWX4fG.PdFehZ4WurQ2w9Fa3WJVbClyLBZ34VgdKhP8195K', NULL, '2022-03-04 05:54:40', '2022-03-04 05:54:40', 0, 'Scofield', NULL, 1, 0, NULL, NULL, 0),
(16, 'Tina', 'tina.tancredi@designsgene.com', NULL, '$2y$10$s6/zp7pQvpM99T8dEjl0xO9xwbmFK9zfsRwio3jMb.jRSUXuGtIie', NULL, '2022-03-04 05:55:34', '2022-03-04 05:55:34', 4, 'Tancredi', NULL, 1, 0, NULL, NULL, 0),
(17, 'George', 'george.carter@designsgene.com', NULL, '$2y$10$QCYEhnenfrjY7HYsC.F8m.yP9/ugWMN24h32afsH396quRvECYCcm', NULL, '2022-03-04 05:56:04', '2022-03-04 05:56:04', 4, 'Carter', NULL, 1, 0, NULL, NULL, 0),
(18, 'Zach', 'zach@designsgene.com', NULL, '$2y$10$kC/qrg2Oa5LrcvPohFKAp.NjkLvIUxnX6SBXV8FJ0EOw5beOVUANG', NULL, '2022-03-04 06:04:52', '2022-03-04 06:04:52', 0, 'Evans', NULL, 1, 0, NULL, NULL, 0),
(19, 'Sean', 'sean.evans@designsgene.com', NULL, '$2y$10$mT8ub6x9ERuIwY3FnOTLnu45PvlC3GxEyUXw.drFDQiNUVgX3hKNu', NULL, '2022-03-04 06:06:05', '2022-03-04 06:06:05', 0, 'Evans', NULL, 1, 0, NULL, NULL, 0),
(20, 'Matt', 'matt.kelly@designsgene.com', NULL, '$2y$10$hpHDQT.fj49lkmwy5dDFHOW7JUKKm7ZXs5iSSHwDYRl747gtfUFQG', NULL, '2022-03-04 06:06:49', '2022-03-04 06:06:49', 4, 'Kelly', NULL, 1, 0, NULL, NULL, 0),
(21, 'Jack', 'jack.brown@designsgene.com', NULL, '$2y$10$5We4jQLpScMF6UNfETdp8uI2ypbthUrrlVrYFNYMvQuczdBQGAoI.', NULL, '2022-03-04 06:07:41', '2022-03-04 06:07:41', 4, 'Brown', NULL, 1, 0, NULL, NULL, 0),
(22, 'Amy', 'amy.white@designsgene.com', NULL, '$2y$10$M.E15o1I0VMrHamjvfK3oOnY3sJoE5esY2DfvfZk5b8qImDlsM4oa', NULL, '2022-03-04 06:08:30', '2022-03-04 06:08:30', 4, 'White', NULL, 1, 0, NULL, NULL, 0),
(23, 'Hannah', 'hannah.austin@designsgene.com', NULL, '$2y$10$5B99OYbPOlnYNw8dZkJTJ.cTnNEfM6rZR0G8tTTHR9rze...ibiFe', NULL, '2022-03-04 06:10:44', '2022-03-04 06:10:44', 4, 'Austin', NULL, 1, 0, NULL, NULL, 0),
(24, 'Jennifer', 'jennifer@designscottage.com', NULL, '$2y$10$IfQl9yi1nt946vPWliemx.vebF6aKLumuswggugPco/TIPv5587Vm', NULL, '2022-03-04 06:12:00', '2022-03-04 06:12:00', 4, 'Hudson', NULL, 1, 0, NULL, NULL, 0),
(25, 'Olivia', 'olivia@designsgene.com', NULL, '$2y$10$bIcW0EmQqySrd9SHHtNjruYf9svk6nrvSsgEW8ZhjaS6Yg4YYQzou', NULL, '2022-03-04 06:12:32', '2022-03-04 06:12:32', 4, 'Olivia', NULL, 1, 0, NULL, NULL, 0),
(26, 'harry', 'harrydev966@gmail.com', NULL, '$2y$10$9LS3IywnFH1NLH0AFgHnKemukxea0RyfoAxovCQxuCNUSgBRLs/6.', NULL, '2022-03-04 06:56:23', '2022-03-04 06:56:23', 3, 'dev', '0123456789', 1, 0, NULL, NULL, 3),
(27, 'Steve', 'steve@gmail.com', NULL, '$2y$10$ZpPQ6oAQ6KwIK884OKO1MOFXLEBIoMRKV1hW1cPTo1I3H8JDM8n0C', NULL, '2022-03-05 01:12:31', '2022-03-05 01:12:31', 4, 'Stark', '7777777', 1, 0, NULL, NULL, 0),
(28, 'Roger', 'roger@gmail.com', NULL, '$2y$10$.CM7AlPYHLaaDTPKiqzKdO5G3a3tiOi8.gg5/VpphEtagDJRc5y5O', NULL, '2022-03-05 01:15:51', '2022-03-05 01:15:51', 0, 'Black', '456', 1, 0, NULL, NULL, 0),
(29, 'Daphne', 'daph@gmail.com', NULL, '$2y$10$r6b5mbUuQzpFdXv6tkYXzOd68MXIcKUfWzT3mnIpmcI5O1szZsMdK', NULL, '2022-03-05 01:19:03', '2022-03-05 01:19:03', 3, 'Mayo', '123131', 1, 0, NULL, NULL, 4),
(30, 'Leo', 'leo.miller@thedesignsplanet.com', NULL, '$2y$10$xUoEAzJkRhgk3fT/2BmJeu5iK7gsng9bgrq1r9P7RpCI3m1HPWUve', NULL, '2022-03-05 02:54:08', '2022-03-05 02:54:08', 0, 'Miller', NULL, 1, 0, NULL, NULL, 0),
(31, 'Jay', 'jay.perkins@thedesignsplanet.com', NULL, '$2y$10$Qoo5T2TqYEjJysGWS5nDce.QsBCCuOjNBc8RFO0asKtdRee.X2Pdq', NULL, '2022-03-05 02:55:01', '2022-03-07 23:51:45', 0, 'Perkins', NULL, 1, 0, NULL, NULL, 0),
(32, 'Max', 'max.williams@thedesignsplanet.com', NULL, '$2y$10$/RUz3sTfDGW0JKfQZ5dPGuyqULFAh9yxIhtagiHUH0qwyWmQaEPRq', NULL, '2022-03-05 02:55:52', '2022-03-05 02:55:52', 0, 'Williams', NULL, 1, 0, NULL, NULL, 0),
(33, 'Danny', 'danny.kealin@thedesignsplanet.com', NULL, '$2y$10$u/y3YFezJN61M7c.PbcZK.EQDx9qbnfi3RH9cxsm18F3lqnoemNpC', NULL, '2022-03-05 02:56:36', '2022-03-05 02:56:36', 4, 'Kealin', NULL, 1, 0, NULL, NULL, 0),
(34, 'Jennifer', 'jennifer@thedesignsplanet.com', NULL, '$2y$10$PaOjCLH/2cba6fHoKZQaGechwxEHB31.MF66vnz2Y7k//mXEcUQq2', NULL, '2022-03-05 02:58:08', '2022-03-05 02:58:08', 0, 'McLaren', NULL, 1, 0, NULL, NULL, 0),
(35, 'tony', 'tony@gmail.com', NULL, '$2y$10$X2izqyyGsoBlNlK6bXAViO5sSxSdyIKmCN/4BfiF53m54XvXjTYO2', NULL, '2022-03-05 05:35:20', '2022-03-05 05:35:20', 0, 'shawn', '46477777', 1, 0, NULL, NULL, 0),
(36, 'john', 'john@gmail.com', NULL, '$2y$10$Y2qQd.yfCamTsM66q/nu2Ox/jRLFMcalmLiPsxe4Vok.urxBrSxhC', NULL, '2022-03-05 05:36:58', '2022-03-05 05:36:58', 4, 'andrew', '898798798797', 1, 0, NULL, NULL, 0),
(37, 'test', 'test3@gmail.com', NULL, '$2y$10$5b0iOIsdq54ixIzjNBNjt.rQQTObaKC3FJJAvkYWjYn/xbRDqwkUK', NULL, '2022-03-05 05:40:22', '2022-03-05 05:40:22', 3, 'check', '1000000', 1, 0, NULL, NULL, 5),
(38, 'Claire', 'clangan@tpi.ca', NULL, '$2y$10$9vlpbG8k/kYhCXCJo9o.1eKI50AXFhJ9nivQoXGJnzqdSoKuTMDuK', NULL, '2022-03-08 04:28:28', '2022-03-08 04:28:28', 3, 'Langan', '2892317905', 1, 0, NULL, NULL, 8),
(39, 'sales', 'sales@crm.com', NULL, '$2y$10$97Mi5nbMTZghssCKepZdDeJtQYJzUPH.V3sd/H1xEeCAVOtE6SdOC', NULL, '2022-03-08 05:03:10', '2022-03-08 05:03:10', 0, 'agent', '123132', 1, 0, NULL, NULL, 0),
(40, 'support', 'support@crm.com', NULL, '$2y$10$axhGTwnvfcHLe37SLW2Fde1urixP9zHzP7tOs3AgbeuaQ2usw0cWK', NULL, '2022-03-08 05:03:34', '2022-03-08 05:03:34', 4, 'agent', '11111111', 1, 0, NULL, NULL, 0),
(41, 'customer', 'musab.technifiedlabs@gmail.com', NULL, '$2y$10$xHoxzxuJaPhQgI16gi3yi.ORGXR2AB7PlIArKg.Wt9Swe9DW3Rnl.', NULL, '2022-03-08 05:14:41', '2022-03-08 05:14:41', 3, 'testa', '7777777', 1, 0, NULL, NULL, 9),
(42, 'production', 'production@crm.com', NULL, '$2y$10$t29UQwvLFS8b5yUrn1z3TeVrvlRAvHloe/XHOFcDloABTN8a2tvkS', NULL, '2022-03-08 05:33:08', '2022-03-08 05:33:08', 1, 'head', '123132', 1, 0, NULL, NULL, 0),
(43, 'prod', 'prodmember@crm.com', NULL, '$2y$10$Ws7Yir6knH/U3V8XBUM6Cu.RNZ8mNaGT/LWXs3ojADBN5ReX.cG5m', NULL, '2022-03-08 05:37:19', '2022-03-08 05:37:19', 5, 'member', '7777777', 1, 0, NULL, NULL, 0),
(44, 'Lisandra', 'lisandramendoza1987@gmail.com', NULL, '$2y$10$GvK6cOlO6h/EkqX3trxewuXRfUfSMwWxHalIvEGh3Ub/RFJ6ZpF2i', NULL, '2022-03-10 00:04:09', '2022-03-10 00:04:09', 3, 'Mendoza', '7868383739', 1, 0, NULL, NULL, 10),
(45, 'Lucas', 'lucas.scott@thedesignsplanet.com', NULL, '$2y$10$cLsVJpt9UzH/e72yF2eVKuk2T/seT.qWi8n91qd4iaZalFzc7paDy', NULL, '2022-03-11 03:57:36', '2022-03-11 03:57:36', 4, 'Scott', NULL, 1, 0, NULL, NULL, 0),
(46, 'test', 'testoneone@gmail.com', NULL, '$2y$10$NpejcWtKdkMZymQUdgKlkOSEUoh9Mc/EtY/WtsiIL3ztJZdh8BKMS', NULL, '2022-03-26 06:53:10', '2022-03-26 06:53:10', 3, 'One', NULL, 1, 0, NULL, NULL, 12);

-- --------------------------------------------------------

--
-- Table structure for table `web_forms`
--

CREATE TABLE `web_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `business_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `decision_makers` text COLLATE utf8mb4_unicode_ci,
  `about_company` text COLLATE utf8mb4_unicode_ci,
  `purpose` text COLLATE utf8mb4_unicode_ci,
  `deadline` text COLLATE utf8mb4_unicode_ci,
  `potential_clients` text COLLATE utf8mb4_unicode_ci,
  `competitor` text COLLATE utf8mb4_unicode_ci,
  `user_perform` text COLLATE utf8mb4_unicode_ci,
  `pages` text COLLATE utf8mb4_unicode_ci,
  `written_content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copywriting_photography_services` tinyint(4) NOT NULL DEFAULT '0',
  `cms_site` tinyint(4) NOT NULL DEFAULT '0',
  `re_design` tinyint(4) NOT NULL DEFAULT '0',
  `working_current_site` tinyint(4) NOT NULL DEFAULT '0',
  `going_to_need` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional_features` text COLLATE utf8mb4_unicode_ci,
  `feel_about_company` text COLLATE utf8mb4_unicode_ci,
  `incorporated` text COLLATE utf8mb4_unicode_ci,
  `need_designed` text COLLATE utf8mb4_unicode_ci,
  `specific_look` text COLLATE utf8mb4_unicode_ci,
  `competition` text COLLATE utf8mb4_unicode_ci,
  `websites_link` text COLLATE utf8mb4_unicode_ci,
  `people_find_business` text COLLATE utf8mb4_unicode_ci,
  `market_site` text COLLATE utf8mb4_unicode_ci,
  `accounts_setup` text COLLATE utf8mb4_unicode_ci,
  `links_accounts_setup` text COLLATE utf8mb4_unicode_ci,
  `service_account` text COLLATE utf8mb4_unicode_ci,
  `use_advertising` text COLLATE utf8mb4_unicode_ci,
  `printed_materials` text COLLATE utf8mb4_unicode_ci,
  `domain_name` text COLLATE utf8mb4_unicode_ci,
  `hosting_account` text COLLATE utf8mb4_unicode_ci,
  `login_ip` text COLLATE utf8mb4_unicode_ci,
  `domain_like_name` text COLLATE utf8mb4_unicode_ci,
  `section_regular_updating` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updating_yourself` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog_written` text COLLATE utf8mb4_unicode_ci,
  `regular_basis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fugure_pages` text COLLATE utf8mb4_unicode_ci,
  `additional_information` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `agent_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_forms`
--

INSERT INTO `web_forms` (`id`, `business_name`, `website_address`, `address`, `decision_makers`, `about_company`, `purpose`, `deadline`, `potential_clients`, `competitor`, `user_perform`, `pages`, `written_content`, `copywriting_photography_services`, `cms_site`, `re_design`, `working_current_site`, `going_to_need`, `additional_features`, `feel_about_company`, `incorporated`, `need_designed`, `specific_look`, `competition`, `websites_link`, `people_find_business`, `market_site`, `accounts_setup`, `links_accounts_setup`, `service_account`, `use_advertising`, `printed_materials`, `domain_name`, `hosting_account`, `login_ip`, `domain_like_name`, `section_regular_updating`, `updating_yourself`, `blog_written`, `regular_basis`, `fugure_pages`, `additional_information`, `user_id`, `invoice_id`, `created_at`, `updated_at`, `agent_id`) VALUES
(1, 'Russell Mack', 'https://www.zyvylanufinas.co', 'Perspiciatis cum se', 'Adipisicing fugit a', 'Ut et et numquam bea', '[\"Bring in new clients to your business\",\"Create a blog that addresses specific topics or interests\",\"Provide support for current clients\"]', 'Reprehenderit maior', 'Ut accusamus molliti', 'Excepteur atque even', '[\"Fill out a contact form\",\"Sign up for your mailing list\"]', '{\"page_name\":[\"Hakeem Bass\",\"Maisie Carr\",\"Xyla Neal\",\"Britanni Garrison\",\"Nita Mcfarland\",\"Brian Mendez\",\"Ursa Garrett\",\"Wyatt Parks\",\"Perry Vincent\",\"Macy Richmond\"],\"content_notes\":[\"Nulla et aperiam nih\",\"Tenetur veritatis eu\",\"Magna nihil dignissi\",\"Est suscipit magni\",\"Adipisicing autem qu\",\"Aliquam consequatur\",\"Consequatur Quidem\",\"Amet et adipisicing\",\"Est possimus autem\",\"Officiis vel porro n\"]}', 'Tempor nobis recusan', 1, 0, 0, 1, '[\"Membership of any kind\"]', 'Qui quos voluptatem', 'Quibusdam saepe comm', 'Voluptatem deserunt', 'Dolores odio porro n', 'Nihil harum natus al', '[\"Ipsum tempor aliquam\",\"Totam delectus eos\",\"Enim excepturi sit\"]', '[\"https:\\/\\/www.pyzonysyxef.info\",\"https:\\/\\/www.kugu.ws\",\"https:\\/\\/www.guh.org\"]', 'Et corporis vel quo', 'Incidunt velit cum', 'Est inventore soluta', 'Sed voluptatum simil', 'Est quidem expedita', 'In perferendis aut i', 'Vitae quia consequat', 'Myles Le', 'Voluptatem Voluptat', 'Ipsam quis in sed te', 'Blaze Beasley', 'Eum in minim nisi in', 'Rerum aut aliqua Pr', 'Culpa temporibus qu', 'Rerum in aut facilis', 'Deserunt ullam et re', 'Dolor quas velit non', 4, 1, '2022-03-04 05:38:53', '2022-03-04 05:39:52', 3),
(2, 'Ezra Dickson', 'https://www.wurumef.com.au', 'Rerum qui eos sint', 'Doloremque non excep', 'Quas et ipsum in ve', '[\"Bring in new clients to your business\",\"Deliver news or calendar of events\",\"Provide your customers with information on a certain subject\",\"Create a blog that addresses specific topics or interests\",\"Provide support for current clients\"]', 'Et nisi dolores blan', 'Vel sint irure adipi', 'Voluptatem voluptate', '[\"Call you\",\"Fill out a quote form\",\"Search for information\"]', '{\"page_name\":[\"Ulric Zimmerman\",\"Doris Stewart\",\"Simon French\",\"Chadwick Chambers\",\"Amaya Vasquez\",\"Darryl Patel\",\"Herrod Burch\",\"Aimee Fox\",\"Talon Dale\",\"Rhonda Jones\"],\"content_notes\":[\"Nostrud eiusmod cill\",\"Nostrum autem et sed\",\"Facere impedit id i\",\"Voluptas eum qui ill\",\"Voluptas dolores ull\",\"Assumenda commodo di\",\"Itaque nisi consecte\",\"In tempore voluptas\",\"Nostrud placeat con\",\"Nulla irure voluptat\"]}', 'Quos delectus archi', 0, 1, 1, 1, '[\"Membership of any kind\"]', 'Reiciendis ea quis d', 'Commodi alias ullamc', 'Sunt ut veritatis es', 'Nam nemo voluptatem', 'Harum omnis vel cum', '[\"Do doloribus eiusmod\",\"Cumque et non quis i\",\"Laborum Nisi ut exe\"]', '[\"https:\\/\\/www.zyhawex.org.au\",\"https:\\/\\/www.likofav.me.uk\",\"https:\\/\\/www.vokagopitotov.cc\"]', 'Est et accusantium', 'Rerum fuga Quo dist', 'Dolor nostrud aut lo', 'Non recusandae Et p', 'Exercitationem obcae', 'Quia impedit dolore', 'Pariatur Explicabo', 'Hilel Barnett', 'Ducimus ad cum ut n', 'Est sit voluptas par', 'Edward Espinoza', 'Nihil nesciunt corp', 'Voluptatum commodi t', 'Elit aliquam veniam', 'Ipsum voluptatem ve', 'Eos ut necessitatib', 'Nemo ut rerum et eli', 6, 2, '2022-03-04 05:46:04', '2022-03-04 05:48:45', 1),
(3, 'Michael Underwood', 'https://www.wubelabajed.cm', 'Molestiae dolores ir', 'Dolorum ea labore te', 'Veniam in lorem dig', '[\"Bring in new clients to your business\",\"Provide your customers with information on a certain subject\"]', 'Dolores optio quasi', 'Nostrud dolor anim a', 'Et in qui iusto et a', 'null', '{\"page_name\":[\"Kendall Matthews\",\"Josephine Valdez\",\"Tate Ray\",\"Odessa Sims\",\"Ferdinand Oconnor\",\"Josiah Carrillo\",\"Molly Collier\",\"Price Fisher\",\"Talon Rosa\",\"Stacey Travis\"],\"content_notes\":[\"Aspernatur cupidatat\",\"Quae fugiat enim no\",\"Consequatur laudant\",\"Id adipisci et non u\",\"Laborum Nisi sit de\",\"Nam est eu tempore\",\"Impedit tempor ipsa\",\"Fugiat nisi velit d\",\"Aut non nisi animi\",\"Tempore aut dolore\"]}', 'Rerum dolores amet', 1, 1, 0, 1, '[\"Membership of any kind\"]', 'Ut elit voluptas qu', 'Fugit magnam volupt', 'Praesentium non faci', 'Provident labore qu', 'Tempor ex laborum ob', '[\"Ut perferendis delec\",\"Reprehenderit volup\",\"Nihil eligendi debit\"]', '[\"https:\\/\\/www.guqygabo.cc\",\"https:\\/\\/www.wuqajulitevo.me.uk\",\"https:\\/\\/www.zalajepohacu.info\"]', 'Iure eos quia minus', 'Ut corrupti vel est', 'Quia praesentium ill', 'Officia consequatur', 'Sint nesciunt conse', 'Et nulla ut totam en', 'Modi elit non nostr', 'Shellie Byrd', 'Elit animi eligend', 'Hic quo natus aut au', 'Emerson Mcguire', 'Et voluptate digniss', 'Ipsum est culpa occ', 'Necessitatibus ut el', 'Omnis aspernatur ita', 'Consequat Illo in l', 'Delectus animi sit', 6, 2, '2022-03-04 05:46:04', '2022-03-04 05:47:51', 1),
(5, 'Briar Arnold', 'https://www.mywimy.cc', 'Eaque ad enim repudi', 'Officia ab omnis in', 'Ducimus dolor hic m', '[\"Explain your products and services\",\"Deliver news or calendar of events\",\"Provide your customers with information on a certain subject\",\"Create a blog that addresses specific topics or interests\",\"Sell a product or products online\"]', 'Repudiandae nihil cu', 'Exercitation esse e', 'Quia proident volup', '[\"Call you\",\"Fill out a quote form\",\"Sign up for your mailing list\"]', '{\"page_name\":[\"Ciara Hogan\",\"Chandler Pate\",\"Kieran Robles\",\"Blaine Richardson\",\"Willa Navarro\",\"Zia Buckner\",\"Susan Grant\",\"Wylie Navarro\",\"Risa Underwood\",\"Meredith French\"],\"content_notes\":[\"Assumenda veniam no\",\"Fugiat delectus qu\",\"Ea officiis obcaecat\",\"Sit unde officia est\",\"Dolorem et laboriosa\",\"Quia quia quae sapie\",\"Perferendis dolores\",\"Sed quisquam dolor e\",\"Minus repudiandae om\",\"Sequi eum ducimus d\"]}', 'Consectetur eius re', 0, 1, 0, 0, 'null', 'Atque quas et ea vit', 'Ut consequatur Sint', 'Quae fugiat obcaecat', 'Aut modi illo anim u', 'Dolor deserunt cupid', '[\"Sed vero quia dicta\",\"Et sunt ut in dolore\",\"Duis temporibus temp\"]', '[\"https:\\/\\/www.qitacotaw.mobi\",\"https:\\/\\/www.bibuzaligiv.com.au\",\"https:\\/\\/www.lohizosegunuq.org\"]', 'Aliquid excepturi ve', 'Enim officia dolore', 'Et modi saepe deseru', 'Repudiandae pariatur', 'Pariatur Quo doloru', 'Voluptatum architect', 'Ipsam tempor consect', 'Nathan Cannon', 'Excepteur deleniti d', 'Laborum dolores cons', 'Shaeleigh Howe', 'Eum quia ut est est', 'Soluta obcaecati exe', 'Aut ut nostrud irure', 'Ducimus excepteur n', 'Ex ipsum dolores ad', 'Ut eu similique labo', 29, 5, '2022-03-05 01:19:03', '2022-03-05 01:23:28', 28),
(6, 'Byron Butler', 'https://www.nomiwoxecobed.me', 'Et voluptatem Et di', 'Quae aut nulla venia', 'Sit voluptatem sit', '[\"Explain your products and services\",\"Bring in new clients to your business\"]', 'Repellendus Dolore', 'Ut molestiae excepte', 'Qui ut unde commodo', '[\"Call you\",\"Purchase a product(s)\"]', '{\"page_name\":[\"Ivory Gill\",\"Angelica Pierce\",\"Sylvester Bernard\",\"Oliver Kline\",\"Bruce Juarez\",\"Jorden Calhoun\",\"Hunter Tyler\",\"Hayley Lowery\",\"Victoria Pate\",\"Noble Owens\"],\"content_notes\":[\"Blanditiis non fugit\",\"Unde exercitationem\",\"Aspernatur exercitat\",\"Eaque incididunt ill\",\"Ullamco dignissimos\",\"Cillum aut et ea qui\",\"Consectetur ea et e\",\"Earum molestiae volu\",\"Officia rerum amet\",\"Soluta ea itaque mod\"]}', 'Dolore placeat dele', 1, 0, 0, 1, '[\"Membership of any kind\"]', 'Voluptate animi deb', 'Aut nisi nisi fugit', 'Est reprehenderit in', 'Dolore officia eveni', 'Esse sit dolorum p', '[\"Ut omnis eos repelle\",\"Fugit perspiciatis\",\"Cupiditate in volupt\"]', '[\"https:\\/\\/www.mazeperisewo.me.uk\",\"https:\\/\\/www.segaqiseqyb.me.uk\",\"https:\\/\\/www.hykafokoty.cm\"]', 'Aliqua Minima est p', 'Enim cupiditate sunt', 'Aut qui aut cupidita', 'Provident in conseq', 'Voluptate voluptas n', 'Aspernatur consequat', 'Id reprehenderit id', 'Dale Sharp', 'Consequatur maiores', 'Atque sequi autem ad', 'Megan Hurst', 'Hic dolore quas illu', 'Qui tempora libero e', 'Et voluptatem Dolor', 'Quia do aliquam quis', 'Ad et et omnis elit', 'Velit commodi pariat', 37, 6, '2022-03-05 05:40:22', '2022-03-05 05:43:36', 35);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand_users`
--
ALTER TABLE `brand_users`
  ADD KEY `brand_users_user_id_foreign` (`user_id`),
  ADD KEY `brand_users_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `category_project`
--
ALTER TABLE `category_project`
  ADD KEY `category_project_project_id_foreign` (`project_id`),
  ADD KEY `category_project_category_id_foreign` (`category_id`);

--
-- Indexes for table `category_users`
--
ALTER TABLE `category_users`
  ADD KEY `category_users_user_id_foreign` (`user_id`),
  ADD KEY `category_users_category_id_foreign` (`category_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clients_user_id_foreign` (`user_id`),
  ADD KEY `clients_brand_id_foreign` (`brand_id`),
  ADD KEY `clients_assign_id_foreign` (`assign_id`);

--
-- Indexes for table `client_files`
--
ALTER TABLE `client_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_files_user_id_foreign` (`user_id`);

--
-- Indexes for table `content_writing_forms`
--
ALTER TABLE `content_writing_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_writing_forms_user_id_foreign` (`user_id`),
  ADD KEY `content_writing_forms_invoice_id_foreign` (`invoice_id`),
  ADD KEY `content_writing_forms_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `create_categories`
--
ALTER TABLE `create_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `form_files`
--
ALTER TABLE `form_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_brand_foreign` (`brand`),
  ADD KEY `invoices_client_id_foreign` (`client_id`);

--
-- Indexes for table `logo_forms`
--
ALTER TABLE `logo_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logo_forms_user_id_foreign` (`user_id`),
  ADD KEY `logo_forms_invoice_id_foreign` (`invoice_id`),
  ADD KEY `logo_forms_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `packages_brand_id_foreign` (`brand_id`),
  ADD KEY `packages_service_id_foreign` (`service_id`),
  ADD KEY `packages_currencies_id_foreign` (`currencies_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projects_user_id_foreign` (`user_id`),
  ADD KEY `projects_client_id_foreign` (`client_id`),
  ADD KEY `projects_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_forms`
--
ALTER TABLE `seo_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seo_forms_user_id_foreign` (`user_id`),
  ADD KEY `seo_forms_invoice_id_foreign` (`invoice_id`),
  ADD KEY `seo_forms_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `smm_forms`
--
ALTER TABLE `smm_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `smm_forms_user_id_foreign` (`user_id`),
  ADD KEY `smm_forms_invoice_id_foreign` (`invoice_id`),
  ADD KEY `smm_forms_agent_id_foreign` (`agent_id`);

--
-- Indexes for table `sub_task`
--
ALTER TABLE `sub_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_task_task_id_foreign` (`task_id`),
  ADD KEY `sub_task_user_id_foreign` (`user_id`),
  ADD KEY `sub_task_assign_id_foreign` (`assign_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_project_id_foreign` (`project_id`),
  ADD KEY `tasks_category_id_foreign` (`category_id`),
  ADD KEY `tasks_user_id_foreign` (`user_id`),
  ADD KEY `tasks_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `web_forms`
--
ALTER TABLE `web_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `web_forms_user_id_foreign` (`user_id`),
  ADD KEY `web_forms_invoice_id_foreign` (`invoice_id`),
  ADD KEY `web_forms_agent_id_foreign` (`agent_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `client_files`
--
ALTER TABLE `client_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `content_writing_forms`
--
ALTER TABLE `content_writing_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `create_categories`
--
ALTER TABLE `create_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `form_files`
--
ALTER TABLE `form_files`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `logo_forms`
--
ALTER TABLE `logo_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `seo_forms`
--
ALTER TABLE `seo_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `smm_forms`
--
ALTER TABLE `smm_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sub_task`
--
ALTER TABLE `sub_task`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `web_forms`
--
ALTER TABLE `web_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `brand_users`
--
ALTER TABLE `brand_users`
  ADD CONSTRAINT `brand_users_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `brand_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_project`
--
ALTER TABLE `category_project`
  ADD CONSTRAINT `category_project_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `create_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_project_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_users`
--
ALTER TABLE `category_users`
  ADD CONSTRAINT `category_users_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `create_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `clients`
--
ALTER TABLE `clients`
  ADD CONSTRAINT `clients_assign_id_foreign` FOREIGN KEY (`assign_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `clients_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `clients_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `client_files`
--
ALTER TABLE `client_files`
  ADD CONSTRAINT `client_files_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `content_writing_forms`
--
ALTER TABLE `content_writing_forms`
  ADD CONSTRAINT `content_writing_forms_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `content_writing_forms_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `content_writing_forms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_brand_foreign` FOREIGN KEY (`brand`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoices_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `logo_forms`
--
ALTER TABLE `logo_forms`
  ADD CONSTRAINT `logo_forms_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `logo_forms_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `logo_forms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `packages`
--
ALTER TABLE `packages`
  ADD CONSTRAINT `packages_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `packages_currencies_id_foreign` FOREIGN KEY (`currencies_id`) REFERENCES `currencies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `packages_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `projects_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `projects_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `seo_forms`
--
ALTER TABLE `seo_forms`
  ADD CONSTRAINT `seo_forms_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `seo_forms_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `seo_forms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `smm_forms`
--
ALTER TABLE `smm_forms`
  ADD CONSTRAINT `smm_forms_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `smm_forms_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `smm_forms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sub_task`
--
ALTER TABLE `sub_task`
  ADD CONSTRAINT `sub_task_assign_id_foreign` FOREIGN KEY (`assign_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `sub_task_task_id_foreign` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`),
  ADD CONSTRAINT `sub_task_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `tasks_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `create_categories` (`id`),
  ADD CONSTRAINT `tasks_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `web_forms`
--
ALTER TABLE `web_forms`
  ADD CONSTRAINT `web_forms_agent_id_foreign` FOREIGN KEY (`agent_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `web_forms_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`),
  ADD CONSTRAINT `web_forms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
